package chess.solver

import org.scalatest._

/**
  * Created by Own on 12/7/2018.
  */
class AppTest extends FunSuite{
  val a = new Application(2,2,List(Rook, Rook) )

  test("get all possible permutation for a  2*2 board with two rooks") {
    assert(a.boards.size == 2)
  }

  test("find the unique scenario where the pieces don't threaten each other on a 2*2 board"){
    assert((a.boards.toList(0)(0).x,a.boards.toList(0)(0).y)  ===(0, 0) )
    assert((a.boards.toList(0)(1).x,a.boards.toList(0)(1).y)  ===(1, 1) )

    assert((a.boards.toList(1)(0).x,a.boards.toList(1)(0).y)  ===(0, 1) )
    assert((a.boards.toList(1)(1).x,a.boards.toList(1)(1).y)  ===(1, 0) )
  }

  test("isAttacked for Rook") {
    assert(a.underAttack(Rook(0, 0), Rook(0, 0)) == true)
    assert(a.underAttack(Rook(0, 0), Rook(1, 0)) == true)
    assert(a.underAttack(Rook(0, 0), Rook(0, 1)) == true)

    assert(a.underAttack(Rook(0, 0), Rook(1, 1)) == false)
  }

  test("isAttacked for Queen") {
    assert(a.underAttack(Queen(0, 0), Queen(2, 0)) == true)
    assert(a.underAttack(Queen(2, 2), Queen(2, 0)) == true)
    assert(a.underAttack(Queen(1, 1), Queen(2, 0)) == true)

    assert(a.underAttack(Queen(0, 1), Queen(2, 0)) == false)
    assert(a.underAttack(Queen(1, 2), Queen(2, 0)) == false)
  }

  test("isAttacked for Rook and Queen") {
    assert(a.underAttack(Rook(0, 0), Queen(2, 0)) == true)
    assert(a.underAttack(Rook(2, 2), Queen(2, 0)) == true)
    assert(a.underAttack(Rook(1, 1), Queen(2, 0)) == true)

    assert(a.underAttack(Rook(0, 1), Queen(2, 0)) == false)
    assert(a.underAttack(Rook(1, 2), Queen(2, 0)) == false)

    assert(a.underAttack(Queen(0, 0), Rook(2, 0)) == true)
    assert(a.underAttack(Queen(2, 2), Rook(2, 0)) == true)
    assert(a.underAttack(Queen(1, 1), Rook(2, 0)) == true)

    assert(a.underAttack(Queen(0, 1), Rook(2, 0)) == false)
    assert(a.underAttack(Queen(1, 2), Rook(2, 0)) == false)
  }

  test("isAttacked for Bishop") {
    assert(a.underAttack(Bishop(0, 0), Rook(2, 0)) == true)
    assert(a.underAttack(Bishop(0, 2), Rook(2, 0)) == true)

    assert(a.underAttack(Bishop(0, 1), Rook(2, 0)) == false)
  }

  test("isAttacked for Knight") {
    assert(a.underAttack(Knight(2, 3), Bishop(3, 1)) == true)
    assert(a.underAttack(Knight(2, 3), Bishop(4, 2)) == true)

    assert(a.underAttack(Knight(2, 3), Bishop(4, 4)) == true)
    assert(a.underAttack(Knight(2, 3), Bishop(3, 5)) == true)
    assert(a.underAttack(Knight(2, 3), Bishop(1, 5)) == true)
    assert(a.underAttack(Knight(2, 3), Bishop(0, 4)) == true)
    assert(a.underAttack(Knight(2, 3), Bishop(0, 2)) == true)
    assert(a.underAttack(Knight(2, 3), Bishop(1, 1)) == true)
  }

  test("isAttacked for King") {
    assert(a.underAttack(King(1, 1), Knight(0, 0)) == true)
    assert(a.underAttack(King(1, 1), Knight(1, 0)) == true)
    assert(a.underAttack(King(1, 1), Knight(2, 0)) == true)
    assert(a.underAttack(King(1, 1), Knight(0, 1)) == true)
    assert(a.underAttack(King(1, 1), Knight(2, 1)) == true)
    assert(a.underAttack(King(1, 1), Knight(0, 2)) == true)
    assert(a.underAttack(King(1, 1), Knight(1, 2)) == true)
    assert(a.underAttack(King(1, 1), Knight(2, 2)) == true)
    assert(a.underAttack(King(1, 1), Knight(1, 1)) == true)
    assert(a.underAttack(King(1, 1), Knight(3, 3)) == false)
  }


}

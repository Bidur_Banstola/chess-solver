package chess.solver

import  org.scalatest._
/**
  * Created by Own on 12/7/2018.
  */
class PieceFactoryTest extends FunSuite{

  test("Factory builds an case class for pieces"){
    assert(PieceFactory.make(Bishop, 0, 0) == Bishop(0, 0))
    assert(PieceFactory.make(King, 0, 0) == King(0, 0))
    assert(PieceFactory.make(Knight, 0, 0) == Knight(0, 0))
    assert(PieceFactory.make(Queen, 0, 0) == Queen(0, 0))
    assert(PieceFactory.make(Rook, 0, 0) == Rook(0, 0))
    }
}

package chess.solver

/**
  * Created by Own on 12/7/2018.
  */
class Application (numOfColumn: Int, numOfRow: Int, pieces: List[(Int, Int) => Piece]){

  type Board= List[Piece]

  val boards: Set[Board] = placeUnits(pieces)

  def placeUnits(k: List[(Int, Int) => Piece]): Set [List[Piece]]= k match {

    case  Nil => Set(List())

    case x :: xs => for {
      pieces <- placeUnits(xs)
      column <- 0 until numOfColumn
      row <- 0 until numOfRow
      piece = PieceFactory.make(x, row, column)
      if isSafe(piece, pieces)
    } yield  (piece:: pieces).sortBy(r => (r.x, r.y))
  }

  def isSafe(piece: Piece, others: List[Piece]) = others forall (!underAttack(piece, _))

  def underAttack(p0: Piece, p1: Piece)= p0.isOffensive(p1) || p1.isOffensive(p0)

  def showResult() {
    val numOfSolutions = boards.size
    val numOfPieces = pieces.groupBy(_.getClass.getSimpleName()).mapValues(_.size).map { case (k, v) => s"$k=$v"}.mkString(", ")
    println(s"$numOfSolutions solutions for $numOfPieces on board $numOfColumn*$numOfRow")
    println()
    println()
    println("Where K stands for king")
    println("      Q stands for Queen")
    println("      B stands for Bishop")
    println("      N stands for Knight")
    // prints n unique boards
    boards.take(3).map(resultBoard(_))                    //Taking only 3 unique outputs
    // boards.map(printBoard(_))                          //For all output; Note: Takes quite a lot of time to complete
  }

  private  def resultBoard(board: Board){
    for (y <- 0 until numOfRow) {
      for (x <- 0 until numOfColumn) {
        board.find(p => p.x == x && p.y == y) match {
          case Some(piece) => print(s"$piece ")
          case None => print(". ")
        }
      }
      println()
    }
    println()
  }



}

package chess.solver

import scala.concurrent.duration._


/**
  * Created by Own on 12/7/2018.
  */

object AppRunner {

  def main(args: Array[String]): Unit = {
    println("Solving the problem with a 7*7 board and few chess-pieces")
    println()
    println()

    val start = System.currentTimeMillis()
    val game = new Application(7, 7, List(King, King, Queen, Queen, Bishop, Bishop, Knight))    //Statically passing data for required results, Could be changed as per the user's liking

    val end = System.currentTimeMillis()
    val elapsed = DurationLong(end - start).millis    //Time period calculation and result in milliseconds

    println(f"Elapsed time ${elapsed.toMinutes} min, ${elapsed.toSeconds % 60} sec, ${elapsed.toMillis % 1000} millis (total ${elapsed.toMillis} millis)")      //changing time period value into higher time units

    game.showResult()                             //Calling a function for showing the output

  }

}

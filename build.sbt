name := "chess-solver"

version := "1.0"

scalaVersion := "2.12.8"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

mainClass in (Compile,run) := Some("chess.solver.AppRunner")

scalacOptions ++= Seq("-optimise")

fork in run := true
javaOptions in run  ++= Seq( "-Xms4g", "-Xmx4g", "-XX:+UseParallelGC", "-XX:+UseParallelOldGC", "-XX:MaxNewSize=3g")

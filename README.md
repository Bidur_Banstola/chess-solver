The problem is to find all unique configurations of a set of normal chess pieces on a chess board with dimensions M×N where none of the pieces is in a position to take any of the others. 
A program which takes as input:

●  The dimensions of the board: M, N 

●  The number of pieces of each type (King, Queen, Bishop, Rook and Knight) to try and place on the board.


As output, the program lists all the unique configurations to the console for which all of the pieces can be placed on the board without threatening each other. The default answer is for a 7×7 board with 2 Kings, 2 Queens, 2 Bishops and 1 Knight. Also the time it took to get the final score is shown as well.

-------------------How to run program for 7×7 board with 2 Kings, 2 Queens, 2 Bishops and 1 Knight---------------

Clone the repository and execute
		
		sbt run

Or you can run AppRunner from Idea Intellij as well. (Prefer the earlier option)

------------Sample Output--------------------------------------------------------------------------------
	
	Solving the problem: M*N board with User Inputed Chess Pieces


	Elapsed time 3 min, 50 sec, 355 millis (total 230355 millis)
	3063828 solutions for Bishop$=2, King$=2, Knight$=1, Queen$=2 on board 7*7


	Where K stands for king
      	  Q stands for Queen
      	  B stands for Bishop
    	  N stands for Knight

	. B . . B . . 
	. N . . . . K 
	. . . . . . . 
	. . . . . . . 
	. . . . K . . 
	Q . . . . . . 
	. . . Q . . .

	. B . . K . . 
	. N . . . . B 
	. . . . . . . 
	. . . . . . . 
	. . . . K . . 
	Q . . . . . . 
	. . . Q . . .

	. N . B . . . 
	. B . . . . . 
	. . . . Q . . 
	. . . . . . . 
	. K . . . K . 
	. . . . . . . 
	. . Q . . . .

-------------------------------------Additional Notes-------------------------------------------------------------------

The program can perform the desired operation for other size of boards and additional chesspieces as well. For that you can have the contents in AppRunner changed as per your requirements (The portion has comments in the code itself and can be found easily). 
By default, the program is only returning three solutions. It can be changed in the showResult function inside the App class. Getting results for all took quite some time therefore only three were taken out.

